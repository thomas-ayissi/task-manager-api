# task-manager-api
A nodejs task manager backend created while taking Andrew Mead's NodeJS developers course.

## Dev Install Instructions
1. Clone the repo.
2. Install [mongoDB](https://www.mongodb.com/download-center/community).
3. Start mongoDB on port 27017.
4. Create a .config dir in the root of the repo. Add a dev.env file with the following ENV VARIABLES defined:

ENV VARIABLE | DESCRIPTION
------------ | ------------
PORT | The port thatyour app will be accesible on. (3000)
SENDGRID_API_KEY | The API key for your [SendGrid](https://sendgrid.com) account.
JWT_SECRET | This is a arbitrary string for encrypting data.
MONGODB_URL | This is the url the server uses to connect to the database. ('mongodb://127.0.0.127017/task-manager-api')

NOTE: DO NOT COMMIT YOUR API KEY OR SECRET TO A REPO!!!!

5. Move into the top level of the repo.
6. Run `npm i` to install all needed packages.
7. Run `npm run dev` to start a local dev server on the port you defined.

## Contributing
If you like this app and want to edit it please let me know! You can fork the repo and submit a PR and I will review it!

## Credit
I highly suggest checking out Andrew Mead's developer courses on [Udemy](https://www.udemy.com/user/andrewmead/)! They are
extremely detailed. Andrew is a great instructor and offers good courses in NodeJS and JavaScript for new developers!
